1. Start a new VM using Centerity image <br/>
<br/>
2. Install and start Docker daemon <br/>
yum install docker -y <br/>
systemctl start docker <br/>
<br/>
3. Download latest gravity : <br/>
wget https://get.gravitational.com/gravity-7.0.28-linux-x86_64-bin.tar.gz  <br/>
<br/>
4. Unzip to a new folder: <br/>
mkdir gravity \
mv gravity-7.0.28-linux-x86_64-bin.tar.gz  gravity <br/>
cd gravity/ <br/>
tar xvf gravity-7.0.28-linux-x86_64-bin.tar.gz <br/>
<br/>
5. Install gravity <br/>
./install.sh <br/>
<br/>
6. Create a new dir with a sample wordpress app.yaml (with openbs enabled <br/>
  mkdir gravity-app <br/>
  cd gravity-app/ <br/>
  git clone https://github.com/gravitational/gravity.git <br/>
  <br/>
 7. Build a Cluster Image <br/>
 tele build -o wordpress-app.tar gravity/examples/wordpress/resources/app.yaml <br/>
 <br/>
 8. Create a clean Linux machine using again the centerity image<br/>
<br/>
9. Unistall iscsi-initiator-utils from CentOs (iscsiadm will run from gravity planet and when installed to CentOS as well it creates conflict) <br/>
yum erase iscsi-initiator-utils -y<br/>
<br/>
10. Install linux kernel module iscsi_tcp for openebs to work properly (cstor creates iscsi targets and iscadm needs to connect to them) <br/>
echo iscsi_tcp > /etc/modules-load.d/openebs.conf <br/>
init 6 <br/>
<br/>
11. Repeat steps 3,4,5, for the new machine<br/>
<br/>
12. Create a new gravity app directory<br/>
mkdir -p /root/gravity/gravity-app<br/>
cd /root/gravity/gravity-app<br/>
<br/>
13. Copy wordpress.tar.gz to this folder and untar it<br/>
 tar xvf wordpress.tar.gz <br/>
 <br/>
14. Create a single-node Kubernetes cluster with Wordpress<br/>
./gravity install --token=secret --cloud-provider=generic<br/>
<br/>
15. If a storage.yaml file is not defined all unmounted devices are used . Other wise define which to include inside this file<br/>
gravity resource create storage.yaml  ## to choose which devices to use<br/>
<br/>
16. Create a storagepool claim that will contain a block device list (that is made from one or more blockdevices)<br/>
 kubectl create -f storagepoolclaim.yaml<br/>
 <br/>
 17. Create a storage class for cstor<br/>
 kubectl create -f  storageclass.yaml<br/>
<br/>
18. Creted a PVC for this storage class and assign this PVC to the pod<br/>
 kubectl create -f pod-pvc.yaml<br/>
 <br/>




ERRORS (solved now)
 <br/>
 6s          Warning   FailedMount              pod/test-pv-pod                     Unable to attach or mount volumes: unmounted volumes=[test-pv-storage default-token-ttrbk], unattached volumes=[test-pv-storage default-token-ttrbk]: timed out waiting for the condition
<br/>


 root     12711 18572  0 20:07 ?        00:00:00 iscsiadm -m discoverydb -t sendtargets -p 100.100.21.165 3260 -I default -o new
<br/>



-bash-4.2# kubectl get po<br/>

NAME                              READY   STATUS              RESTARTS   AGE<br/>

test-pv-pod                       0/1     ContainerCreating   0          32m

<br/>




