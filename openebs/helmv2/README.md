openebs using helmv2

Check steps.txt


------

https://openebs.io/get-started



OpenEBS supports a range of storage engines so that developers can deploy the storage technology appropriate to their application design objectives. - 

- Distributed applications like Cassandra can use the LocalPV engine for lowest latency writes.
- Monolithic applications like MySQL and PostgreSQL can use Mayastor or cStor based on ZFS for resilience. 
- Streaming applications like Kafka can use the NVMe engine Mayastor for best performance in edge environments.

 Across engine types, OpenEBS provides a consistent framework for high availability, snapshots, clones and manageability.

Types of OpenEBS Storage Engines

OpenEBS is a collection Storage Engines, allowing you to pick the right storage solution for your Stateful workloads and the type of Kubernetes platform. **At a high-level, OpenEBS supports two broad categories of volumes - Local and Replicated**.

**Local Volumes**

**Local Volumes are accessible only from a single node in the cluster**. Pods using Local Volume have to be scheduled on the node where volume is provisioned. Local Volumes are typically preferred for distributed workloads like Cassandra, MongoDB, Elastic, etc that are distributed in nature and have high availability built into them.

Depending on the type of storage attached to your Kubernetes worker nodes, you can select from different flavors of Dynamic Local PV - Hostpath, Device, ZFS or Rawfile.

**Replicated Volumes (aka Highly Available Volumes)**

Replicated Volumes as the name suggests, are those that have their data synchronously replicated to multiple nodes. Volumes can sustain node failures. The replication also can be setup across availability zones helping applications move across availability zones.

Replicated Volumes also are capable of enterprise storage features like snapshots, clone, volume expansion and so forth. Replicated Volumes are a preferred choice for Stateful workloads like Percona/MySQL, Jira, GitLab, etc.

Depending on the type of storage attached to your Kubernetes worker nodes and application performance requirements, you can select from Jiva, cStor or Mayastor.
