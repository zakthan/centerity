This repo has been tested for k8s v1.13

**Helm v2 instructions:**

- Git clone this repo
- cd inside this repo and cd to helmv2 folder
- Edit the desired ip range for the LB inside metallb-configmap.yaml
- bash -x steps.txt (this will install metallb speaker and controller v0.9.5 and a test LB service/deployment)
- kugectl get svc -> check the IP of the LB service and try to fetch the test web page with curl to confirm that it works



**Helm v3 instructions:**

After VM is ready and app is intalled:

- NodeSelector for metallb controller deployment and metallb speaker daemonset -> kubernetes.io/os=linux needs to beta.kubernetes.io/os=linux (it is hardcoded at the template. Needs to be changed for speaker daemonset and controller deployment)


1. Workaround for correct label :

**kubectl label node <node_name> kubernetes.io/os=linux**

2. Clone this repo and cd to folder of the repo 

3. To install helmv3 type :

**bash -x install-helm3.sh**

4. Type an IP/IP pool at **adrresses** parameter of **metallb-configmap.yaml** that is within the range of IPs of your network card 

5. To install metallb,nginx ingress controller and a helloworld service/deployment type:

**bash -x steps.txt**

6. From metallb daemonset delete all the lines after **securityContext.capabilities** (including capabilities. For this to be permanent template needs to be edited either deleting these lines or puting a control for k8s v1.13)

7. To test that it works:

Add an entry at/etc/hosts  -> <metallb ip> zak.example.com

curl http://zak.example.com







