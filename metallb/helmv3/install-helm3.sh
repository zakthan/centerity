curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3
chmod 700 get_helm.sh
./get_helm.sh
kubectl config view --raw >~/.kube/config
chmod go-r /root/.kube/config

##in order to use special command for helm v3
cp /usr/local/bin/helm /usr/local/bin/helm3
